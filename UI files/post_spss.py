import os
import requests
import json

os.chdir(r'C:\\Users\\altergc\\Documents\\ICPSR\\Project development\\metadata_capture\\scripts\\Small_test')
## os.chdir(r'C:\\Users\\altergc\\Documents\\ICPSR\\Project development\\metadata_capture\\scripts\\SPSS_scripts')


# *****  get variable names from Updater ***********

APIendpoint=  "http://52.90.103.106:8090/dataset-updater-ws/api/info/variables/names"

var_response = requests.post(url = APIendpoint, files={"file": open('36437-0001subset.xml', mode='rb') } )

description1 = {
    "input_file_name": "36437-0001subset.sav",
    "DDI_XML_file": "36437-0001subset.xml",
    "file_name_DDI": None,
    "variables": var_response.json()
    }


print("****** data description 1 ********")
print(description1)

##description2 =  {   
##    "input_file_name": "temps.csv",
##    "DDI_XML_file": None,
##    "file_name_DDI": None,
##    "variables": var_names2  }

##print("****** data description 2 ********")
##print(description2)


script_name='compute1'

with open(script_name +'.sps', mode='r') as sc1:
     sc2 = sc1.readlines()


script=""
script = script.join(sc2)
print("******** script **********")
print(script)



parameters = { "data_file_descriptions": [], "SPSS": {} }


parameters["data_file_descriptions"].append(description1)
##parameters["data_file_descriptions"].append(description2)

parameters["SPSS"] = script 

print("******** parameters *******")
print(parameters)



content = { "parameters": parameters }

print("****** content *********")

## print(content)
print( json.dumps(content, indent=4) )


with open('sdtl_spss_'+script_name + '.json', mode='w') as InputFile:
    InputFile.write( json.dumps(content, indent=4) )


APIendpoint=  "http://52.90.103.106:8080/api/spsstosdtl"

sdtlOut = requests.post(url = APIendpoint, data= json.dumps(content, indent=4) )
print(sdtlOut.text)







