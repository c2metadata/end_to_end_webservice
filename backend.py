"""
API Controller for the end to end webservice.

Copyright 2020 Regents of the University of Michigan

This software is the result of collaborative efforts from all
participants of the C2Metadata project (http://www.c2metadata.org)

C2Metadata is supported by the Data Infrastructure Building
Blocks (DIBBs) program of the National Science Foundation through
grant NSF ACI-1640575.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

File created in 2020 by Alexander Mueller.
"""

import flask
import glob
import json
import os
import requests
import subprocess
import time
import traceback
import waitress
import xml.etree.ElementTree

from werkzeug.exceptions import *
from endpoints import *

app = flask.Flask(__name__)
app.secret_key = os.urandom(64)

# delete any generated files older than 30 minutes
@app.before_request
def before_request():
    now = time.time()

    try:
        for file in glob.glob("generated_files/*"):
            timestamp = float(file.split("/")[1].split("-")[0])
            if (now - timestamp) > (30 * 60):
                subprocess.check_output(f"rm -rf {file}", shell=True)
    except KeyError:
        pass
    except ValueError:
        pass


# Overriding the default 500 error in cases where we have more information
class Upstream500Error(InternalServerError):

    def __init__(self, error, filetype, output, service, project):
        self.stacktrace = ''.join(traceback.format_exception(None, error, error.__traceback__))
        self.filetype = filetype    # e.g. XML
        self.output = output        # what the failing service returned
        self.service = service      # the name of the service that failed
        self.project = project      # the URL of the relevant GitLab project (for bug reports)

class Forwarded500Error(InternalServerError):

    def __init__(self, previous, current, previous_project, current_project, status, messages):
        self.previous = previous                    # the previous service with bad output
        self.current = current                      # the current service with the error
        self.previous_project = previous_project    # GitLab URL for the previous service code
        self.current_project = current_project      # GitLab URL for the current service code
        self.status = status                        # HTTP status returned by current service
        self.messages = messages                    # error messages returned by current service

# Overriding the default 503 error to include the proper place to file bug reports
class APIError(ServiceUnavailable):
    
    def __init__(self, error, service, project):
        self.stacktrace = ''.join(traceback.format_exception(None, error, error.__traceback__))
        self.service = service      # the name of the API that failed
        self.project = project      # the URL of the relevant GitLab project (for bug reports)

def remove_ddi():
    for file in glob.glob("*.xml"):
        subprocess.check_output(f"rm -rf {file}", shell=True)

def get_code(request):
    code = flask.request.files["codeFile"].read().decode()
    if len(code) == 0:
        raise BadRequest(description="Error: a script file is required for translation to SDTL.")

    # This would be the place to check if the filetype specified by the user
    # matches the file extension of the script file they actually uploaded

    return code

# construct the data file descriptions in the format the parsers expect
def get_descriptions(request):
    descriptions = []
    for x in range(4):
        try:
            ddi_xml_file = request.files[f"ddiFile{x}"].filename

            try:
                file_name_DDI = request.form[f"ddi_filename{x}"]
            except KeyError:
                file_name_DDI = None

            # write out the file stream to a temporary file for use in later requests
            with open(ddi_xml_file, "w") as input_ddi:
                input_ddi.write(request.files[f"ddiFile{x}"].read().decode())

            # get the per-file variable list for each of the data files
            variables = requests.post(url=VARIABLES, files={"file": open(ddi_xml_file, mode="rb")})
            variables.raise_for_status()    # throw an error if the status is not 200

            description = {
                "input_file_name": request.form[f"script_filename{x}"],
                "DDI_XML_file": ddi_xml_file,
                "file_name_DDI": file_name_DDI,
                "variables": variables.json()
            }
            descriptions.append(description)

        # The form takes between 1 and 4 data/DDI files,
        # so it's ok if fields 2 through 4 are blank
        except FileNotFoundError:
            if x == 0:
                raise BadRequest(description="Error: at least one data filename and DDI file are required for the updater.")
            break
    return descriptions

def parse(language, code, descriptions):
    # The parsers expect an input JSON string in this format
    parser_input = {
        "parameters": {
            "data_file_descriptions": descriptions,
            language: code
        }
    }
    
    # try to call the relevant language parser
    try:
        sdtl = requests.post(url=PARSERS[language], data=json.dumps(parser_input))

    # if the connection fails, raise an APIError with the relevant information
    except requests.exceptions.ConnectionError as conn_error:
        remove_ddi()
        raise APIError(error=conn_error,
                       service=f"{language} parser",
                       project=PARSER_PROJECTS[language])

    return sdtl

def write_sdtl(sdtl, now, language):
    # The generated file includes the timestamp from earlier for uniqueness
    with open(f"generated_files/{now}-sdtl.json", mode="w") as sdtl_file:

        # try to format the JSON from the SDTL string that was returned
        try:
            sdtl_json = json.loads(sdtl.text)
            sdtl_file.write(json.dumps(sdtl_json, indent=4))

        # if it's not valid JSON, raise our Upstream500Error with the relevant info
        except json.decoder.JSONDecodeError as json_error:
            remove_ddi()
            raise Upstream500Error(error=json_error,
                                 filetype="JSON",
                                 output=sdtl.text,
                                 service=f"{language} parser",
                                 project=PARSER_PROJECTS[language])

def get_datasets(descriptions):
    # if there's only one data file, the format of the descriptions is simple
    if len(descriptions) == 1:
        datasets = f"-F 'datasets=@./{descriptions[0]['DDI_XML_file']}' "

    # otherwise, we need some metadata to distinguish the different files
    else:
        input_info = [
            {
                "scriptName": description["input_file_name"],
                "xmlName": description["file_name_DDI"],
                "primary": False,
                "position": None
            }
            for description in descriptions
        ]

        with open("inputInfo.json") as info_file:
            info_file.write(json.dumps(input_info, indent=4))

        datasets_list = [
            f"-F 'datasets=@./{description['DDI_XML_file']}'"
            for description in descriptions
        ]
        datasets_list.append(f"-F 'inputInfo=@./inputInfo.json'")

        datasets = " ".join(datasets_list)

    return datasets

def update(datasets, now, language):
    # This has to be run as a CURL command in the shell because the
    # requests library doesn't support the ";type=application/json" part
    curl = (
        f"curl -X POST '{UPDATER}' "
        f"-F 'sdtl=@./generated_files/{now}-sdtl.json;type=application/json' "
        f"{datasets} "
        f"-H 'Content-Type: multipart/form-data'"
    )
    xml_out = subprocess.check_output(curl, shell=True).decode()

    # if the output is not valid XML, check if there's a JSON error document
    # if there is, process and display it; otherwise throw a custom error
    try:
        root = xml.etree.ElementTree.fromstring(xml_out)
    except xml.etree.ElementTree.ParseError as xml_error:
        remove_ddi()
        try:
            updater_json = json.loads(xml_out)
            raise Forwarded500Error(previous=f"{language} parser",
                                    current="Dataset Updater Webservice",
                                    previous_project=PARSER_PROJECTS[language],
                                    current_project=UPDATER_PROJECT,
                                    status=updater_json["code"],
                                    messages=updater_json["errors"])
        except json.decoder.JSONDecodeError as json_error:
            raise Upstream500Error(error=xml_error, 
                                   filetype="XML",
                                   output=xml_out,
                                   service="Dataset Updater Webservice", 
                                   project=UPDATER_PROJECT)

    # This would be the place to validate the XML against the DDI 2.5 schema

    # otherwise, write out the updated DDI to a new generated file
    with open(f"generated_files/{now}-updated_ddi.xml", mode="w") as updated_ddi:
        updated_ddi.write(xml_out)

    # remove the intermediate DDI file(s) that were created before the call to the updater
    remove_ddi()

def codebook(now):
    # Call the codebook formatter with the updated DDI
    with open(f"generated_files/{now}-updated_ddi.xml", mode="rb") as updated_ddi:
        codebook = requests.post(url=CODEBOOK, files={"ddiFile": updated_ddi})

    # This would be the place to check if the contents of the codebook are valid

    # Write out the codebook to a new generated file
    with open(f"generated_files/{now}-codebook.html", mode="w") as codebook_file:
        codebook_file.write(codebook.text)

# GET returns the form page; POST sends the form here to be handled
@app.route("/", methods=["GET", "POST"])
def record():

    if flask.request.method == "POST":

        # get the current time to include in generated filenames
        # (this avoids race conditions and overlapping filenames between users)
        now = time.time()

        language = flask.request.form["mode"]
        ddi_type = flask.request.form["type"]
        if ddi_type != "xml":
            raise werkzeug.exceptions.NotImplemented(description="Error: this application only supports DDI in XML format.")

        # For proper error reporting, we need to read the code file before everything else
        code = get_code(flask.request)

        descriptions = get_descriptions(flask.request)

        sdtl = parse(language, code, descriptions)

        write_sdtl(sdtl, now, language)

        datasets = get_datasets(descriptions)

        update(datasets, now, language)

        codebook(now)

        # render the Thank You page, which contains links to the generated files
        return flask.render_template("Thank You.htm", now=now, base=HTML_BASE, static=STATIC_PATH, generated=GENERATED_PATH)

    else:
        # render the c2metadata page, which has the input form
        return flask.render_template("c2metadata.htm", base=HTML_BASE, static=STATIC_PATH)

# force the linked files to be downloaded rather than viewed
@app.route(f"/{GENERATED_PATH}/<file>", methods=["GET"])
def get_file(file):
    return flask.send_file(f"{GENERATED_PATH}/{file}", as_attachment=True)

# Error handlers

# 400 Bad Request
@app.errorhandler(400)
def bad_request(e):
    return flask.render_template("400.html", base=HTML_BASE, static=STATIC_PATH, description=e.description), 400

# 404 Not Found
@app.errorhandler(404)
def page_not_found(e):
    e.description = "The page you were looking for does not exist on this server."
    return flask.render_template("404.html", base=HTML_BASE, static=STATIC_PATH, description=e.description), 404

# 500 Internal Server Error
@app.errorhandler(500)
def internal_server_error(e):
    # If this is a Upstream500Error, return the upstream 500 error document
    if hasattr(e, "filetype"):
        return flask.render_template("upstream500.html", base=HTML_BASE, static=STATIC_PATH, error=e), 500

    # similarly if this is a 500 error forwarding another error, render that document
    elif hasattr(e, "previous"):
        return flask.render_template("forwarded500.html", base=HTML_BASE, static=STATIC_PATH, error=e), 500

    # otherwise, return the general 500 error document
    else:
        error = e.original_exception
        e.original_exception = ''.join(traceback.format_exception(None, error, error.__traceback__))
        return flask.render_template("general500.html", base=HTML_BASE, static=STATIC_PATH, error=e), 500

# 503 Service Unavailable
@app.errorhandler(503)
def api_error(e):
    return flask.render_template("503.html", base=HTML_BASE, static=STATIC_PATH, error=e), 503

# development server
#app.run(host="127.0.0.1", port=8096, debug=True)

# production server
waitress.serve(app, host="0.0.0.0", port=8096)
