"""
File containting URL constants to be used by the backend.

Copyright 2020 Regents of the University of Michigan

This software is the result of collaborative efforts from all
participants of the C2Metadata project (http://www.c2metadata.org)

C2Metadata is supported by the Data Infrastructure Building
Blocks (DIBBs) program of the National Science Foundation through
grant NSF ACI-1640575.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

File created in 2020 by Alexander Mueller.
"""

HTML_BASE = "/updater/"
STATIC_PATH = "static"
GENERATED_PATH = "generated_files"

WEBSERVER_ADDRESS = "http://c2metadata.mtna.us"

# change these if the paths or ports change
PARSERS = {
    "spss":     f"{WEBSERVER_ADDRESS}/api/spss",
    "r":        f"{WEBSERVER_ADDRESS}/api/r",
    "sas":      f"{WEBSERVER_ADDRESS}/api/sas",
    "stata":    f"{WEBSERVER_ADDRESS}/api/stata",
    "python":   f"{WEBSERVER_ADDRESS}/api/python"
}

UPDATER = f"{WEBSERVER_ADDRESS}/api/updater/update/xml"
VARIABLES = f"{WEBSERVER_ADDRESS}/api/updater/api/info/variables/names"
VALIDATOR = f"{WEBSERVER_ADDRESS}/api/updater/validate"

CODEBOOK = f"{WEBSERVER_ADDRESS}/api/codebook"

PSEUDOCODE = f"{WEBSERVER_ADDRESS}/api/pseudocode"

# GitLab URLs to show in error messages so that issues are filed appropriately
PARSER_PROJECTS = {
	"spss":		"https://gitlab.com/c2metadata/sdtl-reader",
	"r":		"https://gitlab.com/c2metadata/sdtl-reader",
	"sas": 		"https://gitlab.com/c2metadata/sas-sdtl",
	"stata": 	"https://gitlab.com/c2metadata/stata-sdtl-converter",
	"python": 	"https://gitlab.com/c2metadata/python-to-sdtl"
}

UPDATER_PROJECT = "https://gitlab.com/c2metadata/dataset-updater-ws"

CODEBOOK_PROJECT = "https://gitlab.com/c2metadata/codebookutil-web"
